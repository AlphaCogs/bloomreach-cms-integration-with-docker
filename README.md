# Bloomreach CMS integration with Docker

this project contains all the scripts and configurations files to complete the tutorial for the Bloomreach CMS dockerisation

This repository contains all the files described in chapter 

# Understanding the Docker file

The folder contains the following

* bin
    * entrypoint.sh
    * setenv.sh
    * wait-for-it.sh
* conf
    * catalina.policy
    * catalina.properties
    * content.xml
    * content.xml.template
    * log4j.xml
    * repository-consistency.xml
    * repository-force.xml
    * repository.xml
    * server.xml

Please refere the article _BloomReach CMS integration with Docker and docker-compose and  optimisation_

